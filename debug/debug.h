/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/18 15:39:59 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 19:51:07 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEBUG_H
# define DEBUG_H
# include "../app/app.h"
# include "../ft_printf/ft_printf.h"
# include "../stack/stack.h"
# include <fcntl.h>
# include <stdarg.h>
# include <unistd.h>

void	debug(t_app *app, const char *format, ...);
void	debug_stacks(t_app *app);

#endif
