/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/18 14:40:55 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 19:50:49 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../app/app.h"
#include "../ft_printf/ft_printf.h"
#include "../stack/stack.h"
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>

void	debug(t_app *app, const char *format, ...)
{
	va_list	ptr;

	if (!format || !(app->flags & DEBUG))
		return ;
	va_start(ptr, format);
	ft_fprintf_va(1, format, &ptr);
	va_end(ptr);
}

void	debug_stacks(t_app *app)
{
	if (!(app->flags & DEBUG))
		return ;
	write(1, "a", 1);
	stack_print(app->a);
	write(1, "b", 1);
	stack_print(app->b);
}
