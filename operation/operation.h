/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operation.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/29 02:46:19 by rauer             #+#    #+#             */
/*   Updated: 2023/05/31 20:43:01 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERATION_H
# define OPERATION_H
# ifndef OPERATION_DIRECT_PRINT
#  define OPERATION_DIRECT_PRINT 1
# endif
# ifndef OPERATION_PRINT_STACK
#  define OPERATION_PRINT_STACK 0
# endif
# include "../app/app.h"
# include "../stack/stack.h"

typedef enum e_operation
{
	_NO_OPERATION = 0,
	SA,
	SB,
	SS,
	PA,
	PB,
	RA,
	RB,
	RR,
	RRA,
	RRB,
	RRR = 11
}			t_operation;

void		operation_print(t_operation o);
int			operation_execute(t_app *app, t_operation o);
int			operation_sa(t_stack *a, t_stack *b);
int			operation_sb(t_stack *a, t_stack *b);
int			operation_ss(t_stack *a, t_stack *b);
int			operation_pa(t_stack *a, t_stack *b);
int			operation_pb(t_stack *a, t_stack *b);
int			operation_ra(t_stack *a, t_stack *b);
int			operation_rb(t_stack *a, t_stack *b);
int			operation_rr(t_stack *a, t_stack *b);
int			operation_rra(t_stack *a, t_stack *b);
int			operation_rrb(t_stack *a, t_stack *b);
int			operation_rrr(t_stack *a, t_stack *b);
char		*operation_string(t_operation o);
t_operation	operation_opposite(t_operation o);
#endif
