/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operation_rrr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/29 02:44:20 by rauer             #+#    #+#             */
/*   Updated: 2023/05/23 20:45:17 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../stack/stack.h"

int	operation_rrr(t_stack *a, t_stack *b)
{
	return (stack_rrotate(a) && stack_rrotate(b));
}
