/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operation.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/09 05:14:25 by rauer             #+#    #+#             */
/*   Updated: 2023/05/31 20:42:53 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../app/app.h"
#include "../ft_printf/ft_printf.h"
#include "../stack/stack.h"
#include "operation.h"
#include <unistd.h>

t_operation	operation_opposite(t_operation o)
{
	t_operation	oppo[13];

	oppo[SA] = SA;
	oppo[SB] = SB;
	oppo[SS] = SS;
	oppo[PA] = PB;
	oppo[PB] = PA;
	oppo[RA] = RRA;
	oppo[RB] = RRB;
	oppo[RR] = RRR;
	oppo[RRA] = RA;
	oppo[RRB] = RB;
	oppo[RRR] = RR;
	if (o < SA || o > RRR)
		return (_NO_OPERATION);
	return (oppo[o]);
}

char	*operation_string(t_operation o)
{
	char	*str[13];

	str[SA] = "sa\n";
	str[SB] = "sb\n";
	str[SS] = "ss\n";
	str[PA] = "pa\n";
	str[PB] = "pb\n";
	str[RA] = "ra\n";
	str[RB] = "rb\n";
	str[RR] = "rr\n";
	str[RRA] = "rra\n";
	str[RRB] = "rrb\n";
	str[RRR] = "rrr\n";
	if (o < SA || o > RRR)
		return ("ERROR");
	return (str[o]);
}

void	operation_print(t_operation o)
{
	ft_printf("%s", operation_string(o));
}

int	operation_execute(t_app *app, t_operation o)
{
	int	(*func[13])(t_stack *, t_stack *);

	func[SA] = operation_sa;
	func[SB] = operation_sb;
	func[SS] = operation_ss;
	func[PA] = operation_pa;
	func[PB] = operation_pb;
	func[RA] = operation_ra;
	func[RB] = operation_rb;
	func[RR] = operation_rr;
	func[RRA] = operation_rra;
	func[RRB] = operation_rrb;
	func[RRR] = operation_rrr;
	return (func[o](app->a, app->b));
}
