/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operation_rr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/29 02:44:20 by rauer             #+#    #+#             */
/*   Updated: 2023/05/23 20:45:05 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../stack/stack.h"

int	operation_rr(t_stack *a, t_stack *b)
{
	return (stack_rotate(a) && stack_rotate(b));
}
