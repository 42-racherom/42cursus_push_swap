/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/26 08:16:59 by racherom          #+#    #+#             */
/*   Updated: 2023/06/01 01:50:48 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "app/app.h"
#include "gnl/gnl.h"
#include "operation/operation.h"
#include "queue/queue.h"
#include "sort/utils/utils.h"
#include "stack/stack.h"
#include <stdlib.h>

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n && s1[i] == s2[i] && s1[i])
		i++;
	if (i < n)
		return ((unsigned char)s1[i] - (unsigned char)s2[i]);
	return (0);
}

int	check_operation(t_app *app, char *str)
{
	t_operation	o;

	o = 0;
	if ((app->flags & DEBUG) > 2)
		debug_stacks(app);
	while (o++ < RRR)
		if (!ft_strncmp(str, operation_string(o), 4))
			break ;
	if (o > RRR)
		return (0);
	if ((app->flags & DEBUG) > 1)
		debug_stacks(app);
	debug(app, operation_string(o));
	return (operation_execute(app, o));
}

int	check(t_app *app)
{
	if (!app->a->len)
		return (1);
	if (app->b->len)
		return (0);
	if (!(app->a->list == smalest(app->a)))
		return (0);
	return (sorted(app->a, NULL, NULL));
}

int	main(int argc, char **argv)
{
	t_app	*app;
	char	*nl;
	int		r;

	app = app_create();
	r = app_parse(app, --argc, ++argv);
	nl = gnl(0);
	while (r && nl)
	{
		r = check_operation(app, nl);
		free(nl);
		nl = gnl(0);
	}
	if (!r)
		write(2, "ERROR\n", 6);
	if (!r)
		return (1);
	debug_stacks(app);
	if (check(app))
		write(1, "OK\n", 3);
	else
		write(1, "KO\n", 3);
	app_free(app);
}
