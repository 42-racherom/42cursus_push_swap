/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 20:14:25 by rauer             #+#    #+#             */
/*   Updated: 2023/05/31 21:02:38 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../list/list.h"
#include "../../stack/stack.h"
#include "utils.h"

int	in_between(int i, t_list *min, t_list *max)
{
	return ((!min || i > min->data) && (!max || i < max->data));
}

int	count(t_stack *s, t_list *min, t_list *max)
{
	t_list	*tmp;
	int		i;

	tmp = s->list;
	i = 0;
	while (tmp)
	{
		if (in_between(tmp->data, min, max))
			i++;
		tmp = tmp->next;
		if (tmp == s->list)
			break ;
	}
	return (i);
}

int	sorted(t_stack *s, t_list *min, t_list *max)
{
	t_list	*tmp;
	int		c;
	int		i;

	if (!s || s->len < 1)
		return (0);
	i = 0;
	if (min)
		tmp = next_bigger(s, min->data);
	else
		tmp = smalest(s);
	if (tmp)
		i = 1;
	else
		return (0);
	c = count(s, min, max);
	while (tmp->next->data > tmp->data && (!max || tmp->next->data < max->data))
	{
		i++;
		tmp = tmp->next;
	}
	return (!c || i == c);
}
