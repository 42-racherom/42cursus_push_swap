/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/23 05:34:10 by racherom          #+#    #+#             */
/*   Updated: 2023/06/01 01:48:57 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "../../app/app.h"
# include "../../list/list.h"
# include "../../stack/stack.h"

t_list	*next_bigger(t_stack *s, int i);
t_list	*next_smaler(t_stack *s, int i);
t_list	*biggest(t_stack *s);
t_list	*smalest(t_stack *s);
t_list	*find_middle(t_stack *s, t_list *min, t_list *max);
int		in_between(int i, t_list *min, t_list *max);
int		count(t_stack *s, t_list *min, t_list *max);
int		sorted(t_stack *s, t_list *min, t_list *max);

#endif
