/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_next.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 20:11:47 by rauer             #+#    #+#             */
/*   Updated: 2023/06/01 01:49:11 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../list/list.h"
#include "../../stack/stack.h"

t_list	*next_bigger(t_stack *s, int i)
{
	t_list	*tmp;
	t_list	*l;

	l = 0;
	tmp = s->list;
	while (tmp)
	{
		if (tmp->data > i && (!l || tmp->data < l->data))
			l = tmp;
		tmp = tmp->next;
		if (tmp == s->list)
			break ;
	}
	return (l);
}

t_list	*next_smaler(t_stack *s, int i)
{
	t_list	*tmp;
	t_list	*l;

	l = 0;
	tmp = s->list;
	while (tmp)
	{
		if (tmp->data < i && (!l || tmp->data > l->data))
			l = tmp;
		tmp = tmp->next;
		if (tmp == s->list)
			break ;
	}
	return (l);
}
