/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/23 05:35:10 by racherom          #+#    #+#             */
/*   Updated: 2023/05/31 20:15:36 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../list/list.h"
#include "../../stack/stack.h"
#include "utils.h"

t_list	*biggest(t_stack *s)
{
	t_list	*tmp;
	t_list	*l;

	l = 0;
	tmp = s->list;
	while (tmp)
	{
		if (!l || tmp->data > l->data)
			l = tmp;
		tmp = tmp->next;
		if (tmp == s->list)
			break ;
	}
	return (l);
}

t_list	*smalest(t_stack *s)
{
	t_list	*tmp;
	t_list	*l;

	l = 0;
	tmp = s->list;
	while (tmp)
	{
		if (!l || tmp->data < l->data)
			l = tmp;
		tmp = tmp->next;
		if (tmp == s->list)
			break ;
	}
	return (l);
}

t_list	*find_middle(t_stack *s, t_list *min, t_list *max)
{
	if (!min)
		min = smalest(s);
	if (!max)
		max = biggest(s);
	while (min != max)
	{
		max = next_smaler(s, max->data);
		if (min == max)
			break ;
		min = next_bigger(s, min->data);
	}
	return (min);
}
