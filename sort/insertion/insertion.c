/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insertion.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/19 01:34:01 by vioma             #+#    #+#             */
/*   Updated: 2023/05/31 20:23:36 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../common/common.h"
#include "../../debug/debug.h"
#include "../../ft_printf/ft_printf.h"
#include "../../list/list.h"
#include "../../operation/operation.h"
#include "../../queue/queue.h"
#include "../../stack/stack.h"
#include "../utils/utils.h"

int	rotation_direction(t_stack *s, int i)
{
	t_list	*t1;
	t_list	*t2;

	if (s->len < 1)
		return (0);
	if (s->list->data == i)
		return (1);
	t1 = s->list->next;
	t2 = s->list->prev;
	while (t1 != t2 && t1->data != i && t2->data != i)
	{
		t1 = t1->next;
		t2 = t2->prev;
	}
	if (t1->data == i)
		return (1);
	if (t2->data == i)
		return (2);
	return (0);
}

int	insertion_sort_a_rotate(t_app *app, int i, t_list *tmp)
{
	int	o;
	int	r;

	debug(app, "Next: %i\n", i);
	r = rotation_direction(app->a, i);
	if (r == 0)
		return (0);
	o = RA;
	if (r > 1)
		o = RRA;
	while (r > 0 && app->a->list->data != i)
	{
		if (tmp && tmp->data == app->a->list->data && tmp->prev->data != i)
		{
			r = queue(app, PB);
			tmp = 0;
		}
		else
			r = queue(app, o);
	}
	return (r);
}

int	insertion_sort_a(t_app *app, t_list *min, t_list *max)
{
	t_list	*tmp;
	t_list	*tmp2;

	common_debug(app, "Insertion Sort A", min, max);
	if (min)
		tmp = next_bigger(app->a, min->data);
	else
		tmp = smalest(app->a);
	if (!tmp)
		return (1);
	if (sorted(app->a, min, max))
		return (common_rotate_a_to(app, tmp->data));
	tmp2 = next_bigger(app->a, tmp->data);
	if (!insertion_sort_a_rotate(app, tmp->data, tmp2) || !queue(app, PB))
		return (0);
	if (!insertion_sort_a(app, min, max))
		return (0);
	if (app->b->list->next->data > app->b->list->data && (!max
			|| app->b->list->next->data < max->data))
		if (!queue(app, SB) || !queue(app, PA))
			return (0);
	return (queue(app, PA));
}

void	insertion_sort_b(t_app *app, t_list *min, t_list *max)
{
	t_list	*tmp;
	int		c;

	c = count(app->b, min, max);
	common_debug(app, "Insertion Sort B", min, max);
	while (c-- > 0)
	{
		if (max)
			tmp = next_smaler(app->b, max->data);
		else
			tmp = biggest(app->b);
		common_rotate_b_to(app, tmp->data);
		queue(app, PA);
	}
	debug_stacks(app);
}
