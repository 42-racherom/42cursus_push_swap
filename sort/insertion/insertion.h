/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insertion.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/23 05:50:59 by racherom          #+#    #+#             */
/*   Updated: 2023/05/31 20:18:50 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSERTION_H
# define INSERTION_H

# include "../../app/app.h"
# include "../../list/list.h"
# include "../../operation/operation.h"
# include "../../stack/stack.h"
# include "../utils/utils.h"

void	insertion_sort_a(t_app *app, t_list *min, t_list *max);
void	insertion_sort_b(t_app *app, t_list *min, t_list *max);

#endif
