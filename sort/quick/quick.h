/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quick.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/23 22:12:36 by racherom          #+#    #+#             */
/*   Updated: 2023/06/01 01:48:02 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef QUICK_H
# define QUICK_H
# ifndef MIN_QUICKSORT
#  define MIN_QUICKSORT 15
# endif

void	quicksort(t_app *app);
void	quicksort_a(t_app *app, t_list *min, t_list *max);
void	quicksort_b(t_app *app, t_list *min, t_list *max);

#endif
