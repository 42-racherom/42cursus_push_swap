/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quick_b.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 23:20:13 by rauer             #+#    #+#             */
/*   Updated: 2023/06/01 01:47:52 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../app/app.h"
#include "../../common/common.h"
#include "../../ft_printf/ft_printf.h"
#include "../../list/list.h"
#include "../../operation/operation.h"
#include "../../queue/queue.h"
#include "../../stack/stack.h"
#include "../insertion/insertion.h"
#include "../utils/utils.h"
#include "quick.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

t_list	*quicksort_b_first(t_app *app, char *str, t_list *min, t_list *max)
{
	t_list	*p;

	common_debug(app, str, min, max);
	if (!app_check_len(app, 0, 2))
		return (NULL);
	if (count(app->b, min, max) < MIN_QUICKSORT)
	{
		insertion_sort_b(app, min, max);
		return (NULL);
	}
	p = find_middle(app->b, min, max);
	debug(app, "p: %i\n", p->data);
	return (p);
}

int	quicksort_b_push(t_app *app, t_list **min, t_list *max)
{
	if (app->b->list == *min)
	{
		queue(app, PA);
		*min = app->a->list;
		if (app->a->len > 1)
			queue(app, RA);
	}
	else if (in_between(app->b->list->data, *min, max))
		queue(app, PA);
	else
		return (0);
	return (1);
}

void	quicksort_b2(t_app *app, t_list *min, t_list *max)
{
	t_list	*p;
	int		i;

	p = quicksort_b_first(app, "Quicksort B1", min, max);
	if (!p)
		return ;
	i = app->b->len;
	while (in_between(app->b->list->prev->data, min, max) && i-- > 0)
	{
		queue(app, RRB);
		quicksort_b_push(app, &p, max);
	}
	quicksort_a(app, p, max);
	queue(app, RRA);
	common_debug(app, "Quicksort B2 MID", min, max);
	debug(app, "p: %i\n", p->data);
	quicksort_b(app, min, p);
	common_debug(app, "Quicksort B2 END", min, max);
	debug(app, "p: %i\n", p->data);
	debug_stacks(app);
}

void	quicksort_b(t_app *app, t_list *min, t_list *max)
{
	int		i;
	t_list	*p;

	p = quicksort_b_first(app, "Quicksort B1", min, max);
	if (!p)
		return ;
	i = app->b->len;
	while (in_between(app->b->list->data, min, max) && i-- > 0)
	{
		if (!quicksort_b_push(app, &p, max))
			queue(app, RB);
	}
	quicksort_a(app, p, max);
	common_debug(app, "Quicksort B MID", min, max);
	debug(app, "p: %i\n", p->data);
	queue(app, RRA);
	quicksort_b2(app, min, p);
	common_debug(app, "Quicksort B END", min, max);
	debug(app, "p: %i\n", p->data);
}
