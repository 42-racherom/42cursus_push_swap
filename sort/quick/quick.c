/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quick.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/21 23:11:59 by racherom          #+#    #+#             */
/*   Updated: 2023/06/01 17:52:49 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../app/app.h"
#include "../../common/common.h"
#include "../../ft_printf/ft_printf.h"
#include "../../list/list.h"
#include "../../operation/operation.h"
#include "../../queue/queue.h"
#include "../../stack/stack.h"
#include "../insertion/insertion.h"
#include "../utils/utils.h"
#include "quick.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int	in_range(t_list *from, t_list *to)
{
	t_list	*tmp;

	if (!from || (to && to->data < from->data))
		return (0);
	tmp = from->next;
	while (tmp && tmp != from && tmp->data > from->data && (!to
			|| tmp->data < to->data))
		tmp = tmp->next;
	return (tmp == to);
}

void	quicksort(t_app *app)
{
	if (!app_check_len(app, 2, 0))
		return ;
	debug(app, "MIN_QUICKSORT: %i\n", MIN_QUICKSORT);
	quicksort_a(app, NULL, NULL);
	common_rotate_a_to(app, smalest(app->a)->data);
}
