/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quick_a.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 23:18:56 by rauer             #+#    #+#             */
/*   Updated: 2023/06/01 00:51:22 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../app/app.h"
#include "../../common/common.h"
#include "../../ft_printf/ft_printf.h"
#include "../../list/list.h"
#include "../../operation/operation.h"
#include "../../queue/queue.h"
#include "../../stack/stack.h"
#include "../insertion/insertion.h"
#include "../utils/utils.h"
#include "quick.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

t_list	*quicksort_a_first(t_app *app, char *str, t_list *min, t_list *max)
{
	t_list	*p;

	common_debug(app, str, min, max);
	if (!app_check_len(app, 2, 0))
		return (NULL);
	if (count(app->a, min, max) < MIN_QUICKSORT)
	{
		insertion_sort_a(app, min, max);
		return (NULL);
	}
	p = find_middle(app->a, min, max);
	debug(app, "p: %i\n", p->data);
	return (p);
}

t_list	*save_p(t_app *app)
{
	t_list	*p;

	queue(app, PB);
	p = app->b->list;
	if (app->b->len > 1)
		queue(app, RB);
	return (p);
}

t_list	*return_p(t_app *app)
{
	if (app->b->len > 1)
		queue(app, RRB);
	queue(app, PA);
	return (app->a->list);
}

void	quicksort_a2(t_app *app, t_list *min, t_list *max)
{
	t_list	*p;
	int		i;

	p = quicksort_a_first(app, "Quicksort A2", min, max);
	if (!p)
		return ;
	i = app->a->len;
	while (in_between(app->a->list->prev->data, min, max) && i-- > 0)
	{
		queue(app, RRA);
		if (app->a->list == p)
			p = save_p(app);
		else if (in_between(app->a->list->data, min, p))
			queue(app, PB);
	}
	quicksort_a(app, p, max);
	p = return_p(app);
	common_debug(app, "Quicksort A2 MID", min, max);
	debug(app, "p: %i\n", p->data);
	quicksort_b(app, min, p);
	common_debug(app, "Quicksort A2 END", min, max);
	debug(app, "p: %i\n", p->data);
}

void	quicksort_a(t_app *app, t_list *min, t_list *max)
{
	t_list	*p;
	int		i;

	p = quicksort_a_first(app, "Quicksort A", min, max);
	if (!p)
		return ;
	i = app->a->len;
	while (in_between(app->a->list->data, min, max) && i-- > 0)
	{
		if (app->a->list == p)
			p = save_p(app);
		else if (in_between(app->a->list->data, p, max))
			queue(app, PB);
		else
			queue(app, RA);
	}
	quicksort_b(app, p, max);
	p = return_p(app);
	common_debug(app, "Quicksort A MID", min, max);
	debug(app, "p: %i\n", p->data);
	quicksort_a2(app, min, p);
	common_debug(app, "Quicksort A END", min, max);
	debug(app, "p: %i\n", p->data);
}
