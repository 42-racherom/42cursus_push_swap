/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 18:44:42 by rauer             #+#    #+#             */
/*   Updated: 2023/06/01 01:49:43 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H
# include "../list/list.h"
# include <stdlib.h>

typedef struct s_stack
{
	int		len;
	t_list	*list;
}			t_stack;

int			stack_append(t_stack *s, int i);
int			stack_push(t_stack *s, int i);
int			stack_pop(t_stack *s);
int			stack_swap(t_stack *s);
int			stack_rotate(t_stack *s);
int			stack_rrotate(t_stack *s);
t_stack		*stack_create(void);
void		stack_free(t_stack *s);
void		stack_print(t_stack *s);

#endif
