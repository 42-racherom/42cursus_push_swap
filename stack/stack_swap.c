/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_swap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/29 02:51:16 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 19:54:05 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

int	stack_swap(t_stack *s)
{
	int	tmp;

	if (s->len < 2 || !s->list || !s->list->next)
		return (0);
	tmp = s->list->data;
	s->list->data = s->list->next->data;
	s->list->next->data = tmp;
	return (1);
}
