/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_push.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/29 03:00:28 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 19:56:02 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../list/list.h"
#include "stack.h"

int	stack_push(t_stack *s, int data)
{
	if (!s || !list_push(&(s->list), data))
		return (0);
	s->len++;
	if (!s->list->prev)
		list_circulate(s->list);
	return (1);
}
