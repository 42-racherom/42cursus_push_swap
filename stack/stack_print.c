/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_print.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/08 03:12:20 by rauer             #+#    #+#             */
/*   Updated: 2023/06/01 01:56:33 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/ft_printf.h"
#include "../list/list.h"
#include "stack.h"
#include <unistd.h>

void	stack_print(t_stack *s)
{
	t_list	*l;

	if (!s)
		return ;
	ft_printf(" %3i|", s->len);
	l = s->list;
	while (l)
	{
		ft_printf(" %3d", l->data);
		l = l->next;
		if (l == s->list)
			break ;
	}
	write(1, "\n", 1);
}
