/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_append.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/29 03:59:04 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 19:59:01 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../list/list.h"
#include "stack.h"

int	stack_append(t_stack *s, int data)
{
	t_list	*tmp;

	if (!s)
		return (0);
	if (s->list)
	{
		tmp = list_last(s->list);
		if (!list_push_behind(&tmp, data))
			return (0);
	}
	else if (!list_push(&s->list, data))
		return (0);
	s->len++;
	return (1);
}
