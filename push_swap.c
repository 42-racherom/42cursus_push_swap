/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/26 19:27:45 by rauer             #+#    #+#             */
/*   Updated: 2023/06/02 18:41:23 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "app/app.h"
#include "common/common.h"
#include "operation/operation.h"
#include "optimize/optimize.h"
#include "queue/queue.h"
#include "sort/quick/quick.h"
#include "sort/utils/utils.h"
#include "stack/stack.h"

void	rotate_to_finish(t_app *app)
{
	t_list	*tmp;

	tmp = smalest(app->a);
	if (tmp)
		common_rotate_a_to(app, tmp->data);
}

void	quicksort(t_app *app);

int	main(int argc, char **argv)
{
	t_app	*app;
	int		r;

	r = 0;
	app = app_create();
	if (!app)
		return (1);
	r = app_parse(app, --argc, ++argv);
	if (!r)
	{
		app_free(app);
		write(2, "ERROR\n", 6);
		return (1);
	}
	debug_stacks(app);
	if (app->a->len < 4)
		common_sort3(app);
	else if (!sorted(app->a, NULL, NULL))
		quicksort(app);
	rotate_to_finish(app);
	optimize(app->q);
	queue_print(app);
	debug_stacks(app);
	app_free(app);
}
