/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 04:14:25 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:38:05 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "ft_printf_intern.h"

int	ft_fprintf_va(int fd, const char *format, va_list *ptr)
{
	int		len;

	if (!format)
		return (0);
	while (*format)
	{
		if (*format != '%')
			len += ft_putchar(fd, *(format++));
		else
		{
			format++;
			format += ft_conversions(fd, ptr, &len, format);
		}
	}
	return (len);
}

int	ft_printf(const char *format, ...)
{
	va_list	ptr;
	int		len;

	if (!format)
		return (0);
	va_start(ptr, format);
	len = ft_fprintf_va(1, format, &ptr);
	va_end(ptr);
	return (len);
}

int	ft_println(const char *line)
{
	int	len;

	len = 0;
	while (line[len])
		len++;
	write(1, line, len);
	write(1, "\n", 1);
	return (len + 1);
}
