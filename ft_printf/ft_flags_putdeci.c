/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_flags_putdeci.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/24 03:49:59 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:34:24 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_decilen(unsigned long i, int precision, int flags, int negativ)
{
	int	len;

	len = 0;
	while (i)
	{
		i /= 10;
		len++;
	}
	if (len < precision)
		len = precision;
	if (flags & 24 || negativ)
		len++;
	return (len);
}

static int	ft_put_sign(int fd, int flags, int negativ)
{
	if (negativ)
		return (ft_putchar(fd, '-'));
	else if (flags & 16)
		return (ft_putchar(fd, '+'));
	else if (flags & 8)
		return (ft_putchar(fd, ' '));
	return (0);
}

static int	ft_putdeci(int fd, unsigned long nbr, int precision)
{
	int	len;

	len = 0;
	if (precision > 1 || nbr > 9)
		len = ft_putdeci(fd, nbr / 10, precision - 1);
	if (precision > 0 || nbr > 0)
		len += ft_putchar(fd, (nbr % 10) + '0');
	return (len);
}

int	ft_flags_putdeci(int fd, unsigned long u, t_flags flags, int negativ)
{
	int				space;
	int				len;

	if (negativ)
		u = (~u) + 1;
	len = ft_decilen(u, flags.precision, flags.flags, negativ);
	space = 0;
	if (flags.width > len)
		space = flags.width - len;
	if (!(flags.flags & 2) && !(flags.flags & 1) && space > 0)
		ft_fill(fd, ' ', space);
	len = ft_put_sign(fd, flags.flags, negativ);
	if (flags.flags & 2 && !(flags.flags & 1) && space > 0)
		ft_fill(fd, '0', space);
	len += ft_putdeci(fd, u, flags.precision);
	ft_flags_after(fd, flags, len);
	return (space + len);
}
