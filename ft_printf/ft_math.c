/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 16:54:00 by racherom          #+#    #+#             */
/*   Updated: 2023/02/13 17:06:01 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

size_t	ft_sqrt(size_t nb)
{
	size_t	i;
	size_t	j;

	if (nb == 0)
		return (0);
	i = 0;
	j = 1;
	while (j <= nb)
	{
		nb -= j++;
		i++;
		j++;
	}
	return (i);
}
