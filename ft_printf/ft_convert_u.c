/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/24 03:39:46 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:32:08 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "ft_printf.h"

int	ft_convert_u(int fd, va_list *ptr, t_flags flags)
{
	unsigned int	i;

	i = va_arg(*ptr, unsigned int);
	return (ft_flags_putdeci(fd, i, flags, 0));
}
