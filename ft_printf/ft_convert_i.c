/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_i.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/24 03:39:46 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:36:06 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "ft_printf.h"

int	ft_convert_i(int fd, va_list *ptr, t_flags flags)
{
	int	i;

	i = va_arg(*ptr, int);
	return (ft_flags_putdeci(fd, i, flags, i < 0));
}
