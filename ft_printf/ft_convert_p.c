/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_p.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/21 21:12:22 by rauer             #+#    #+#             */
/*   Updated: 2023/05/18 15:31:42 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "ft_printf.h"

int	ft_convert_p(int fd, va_list *ptr, t_flags flags)
{
	unsigned long	i;

	i = va_arg(*ptr, unsigned long);
	flags.flags |= 4;
	return (ft_flags_puthex(fd, i, flags, 'a'));
}
