/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app_parse.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/18 13:33:16 by rauer             #+#    #+#             */
/*   Updated: 2023/06/02 20:49:37 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/ft_printf.h"
#include "../libft/libft.h"
#include "../list/list.h"
#include "../stack/stack.h"
#include "app.h"

int	_atoi(const char *str, int *i)
{
	int	m;
	int	j;

	*i = 0;
	m = 1;
	while ((*str >= 9 && *str <= 13) || *str == ' ')
		str++;
	if (*str == '+')
		str++;
	if (*str == '-')
	{
		m = -1;
		str++;
	}
	while (ft_isdigit(*str))
	{
		j = (*i * 10) + (m * (*(str++) - '0'));
		if ((m == 1 && j < *i) || (m == -1 && j > *i))
			return (0);
		*i = j;
	}
	if (*str)
		return (0);
	return (1);
}

void	app_parse_flag(t_app *app, char *arg)
{
	if (!arg || !app_check(app) || *(arg++) != '-')
		return ;
	while (*arg)
	{
		if (*arg == 'd' && (app->flags & DEBUG) < DEBUG)
			app->flags++;
		arg++;
	}
}

int	app_parse(t_app *app, int argc, char **argv)
{
	t_list	*tmp;
	int		nbr;
	int		r;

	if (!app_check(app))
		return (0);
	r = 1;
	while (r > 0 && argc > 0 && argv && *argv && **argv == '-'
		&& ft_isalpha((*argv)[1]))
	{
		app_parse_flag(app, *(argv++));
		argc--;
	}
	while (r > 0 && argc > 0)
	{
		r = _atoi(*(argv++), &nbr);
		tmp = list_find(app->a->list, nbr);
		if (!r || tmp)
			return (0);
		r = stack_append(app->a, nbr);
		argc--;
	}
	list_circulate(app->a->list);
	return (r);
}
