/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/18 12:50:33 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 20:46:02 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef APP_H
# define APP_H
# include "../stack/stack.h"
# define DEBUG 03

typedef struct s_app
{
	t_stack	*a;
	t_stack	*b;
	t_stack	*q;
	int		flags;
}			t_app;

void		app_free(t_app *app);
int			app_check(t_app *app);
int			app_check_len(t_app *app, int a, int b);
t_app		*app_create(void);
int			app_parse(t_app *app, int argc, char **argv);

#endif
