/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   app.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/18 13:01:33 by rauer             #+#    #+#             */
/*   Updated: 2023/06/02 20:46:51 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../stack/stack.h"
#include "app.h"
#include <stdlib.h>

void	app_free(t_app *app)
{
	if (!app)
		return ;
	if (app->a)
		stack_free(app->a);
	if (app->b)
		stack_free(app->b);
	if (app->q)
		stack_free(app->q);
	free(app);
}

t_app	*app_create(void)
{
	t_app	*app;

	app = malloc(sizeof(t_app));
	if (!app)
		return (0);
	app->flags = 0;
	app->a = stack_create();
	if (!app->a)
		return (0);
	app->b = stack_create();
	if (!app->b)
		return (0);
	app->q = stack_create();
	if (!app->q)
		return (0);
	return (app);
}

int	app_check(t_app *app)
{
	return (app && app->a && app->b && app->q);
}

int	app_check_len(t_app *app, int a, int b)
{
	return (app_check(app) && app->a->len >= a && app->b->len >= b);
}
