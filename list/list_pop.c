/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_pop.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 19:34:36 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 21:34:51 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

int	list_pop(t_list **l)
{
	int		data;
	t_list	*next;
	t_list	*prev;

	if (!l || !*l)
		return (0);
	data = (*l)->data;
	next = (*l)->next;
	prev = (*l)->prev;
	if (prev)
		prev->next = next;
	if (next)
		next->prev = prev;
	if (next == *l)
		next = NULL;
	if (prev == *l)
		prev = NULL;
	free(*l);
	*l = next;
	if (!*l)
		*l = prev;
	return (data);
}
