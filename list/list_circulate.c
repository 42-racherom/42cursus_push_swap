/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_circulate.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/30 02:47:43 by racherom          #+#    #+#             */
/*   Updated: 2023/05/26 21:34:45 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void	list_circulate(t_list *l)
{
	t_list	*last;
	t_list	*first;

	if (!l)
		return ;
	first = l;
	last = l;
	while ((first->prev || last->next) && last->next != first)
	{
		if (first->prev)
			first = first->prev;
		if (last->next)
			last = last->next;
	}
	if (last->next == first)
		return ;
	last->next = first;
	first->prev = last;
}
