/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_clear.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 19:50:24 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 21:34:42 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void	list_clear(t_list *l, void (*del)(int))
{
	t_list	*tmp;

	if (!l)
		return ;
	if (l->prev)
		l->prev->next = NULL;
	while (l)
	{
		tmp = l->next;
		if (del)
			del(l->data);
		free(l);
		l = tmp;
	}
}
