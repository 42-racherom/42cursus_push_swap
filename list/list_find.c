/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_find.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/14 15:49:24 by racherom          #+#    #+#             */
/*   Updated: 2023/05/26 21:34:40 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_list	*list_find(t_list *l, int data)
{
	t_list	*tmp;

	tmp = l;
	while (tmp && tmp->data != data)
	{
		tmp = tmp->next;
		if (tmp == l)
			break ;
	}
	if (tmp && tmp->data == data)
		return (tmp);
	return (NULL);
}
