/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_push.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 19:11:35 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 21:34:59 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"
#include <stdlib.h>

t_list	*list_push(t_list **l, int data)
{
	t_list	*new;

	new = malloc(sizeof(t_list));
	if (!new)
		return (NULL);
	new->data = data;
	new->prev = 0;
	new->next = 0;
	if (!l)
		return (new);
	if (*l)
	{
		if ((*l)->prev)
			(*l)->prev->next = new;
		new->prev = (*l)->prev;
		(*l)->prev = new;
	}
	new->next = *l;
	*l = new;
	return (new);
}

t_list	*list_push_behind(t_list **l, int data)
{
	t_list	*new;

	new = malloc(sizeof(t_list));
	if (!new)
		return (NULL);
	new->data = data;
	new->prev = 0;
	new->next = 0;
	if (!l)
		return (new);
	new->prev = *l;
	if (*l)
	{
		if ((*l)->next)
			(*l)->next->prev = new;
		new->next = (*l)->next;
		(*l)->next = new;
	}
	else
		*l = new;
	return (new);
}
