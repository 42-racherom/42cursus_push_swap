/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 18:57:46 by rauer             #+#    #+#             */
/*   Updated: 2023/06/01 01:57:19 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H
# include <stdlib.h>

typedef struct s_list
{
	int				data;
	struct s_list	*next;
	struct s_list	*prev;
}					t_list;

int					list_pop(t_list **l);
void				list_clear(t_list *l, void (*del)(int));
int					list_max(t_list *l, int (*compare)(int, int));
void				list_circulate(t_list *l);
t_list				*list_last(t_list *l);
t_list				*list_find(t_list *l, int data);
t_list				*list_push(t_list **l, int data);
t_list				*list_push_behind(t_list **l, int data);

#endif
