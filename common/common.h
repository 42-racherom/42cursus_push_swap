/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 19:53:22 by rauer             #+#    #+#             */
/*   Updated: 2023/05/31 20:43:39 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

# include "../debug/debug.h"
# include "../list/list.h"
# include "../operation/operation.h"
# include "../queue/queue.h"
# include "../stack/stack.h"

int		common_rotate_a_to(t_app *app, int i);
int		common_rotate_b_to(t_app *app, int i);
int		common_sort3(t_app *app);
void	common_debug(t_app *app, char *str, t_list *min, t_list *max);

#endif
