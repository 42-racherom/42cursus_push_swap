/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 19:43:24 by rauer             #+#    #+#             */
/*   Updated: 2023/06/01 02:05:02 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../debug/debug.h"
#include "../list/list.h"
#include "../operation/operation.h"
#include "../queue/queue.h"
#include "../stack/stack.h"

int	common_rotation_direction(t_stack *s, int i)
{
	t_list	*t1;
	t_list	*t2;

	if (s->len < 1)
		return (0);
	if (s->list->data == i)
		return (1);
	t1 = s->list->next;
	t2 = s->list->prev;
	while (t1 != t2 && t1->data != i && t2->data != i)
	{
		t1 = t1->next;
		t2 = t2->prev;
	}
	if (t1->data == i)
		return (1);
	if (t2->data == i)
		return (2);
	return (0);
}

int	common_rotate_a_to(t_app *app, int i)
{
	t_list	*t1;
	t_list	*t2;
	int		r;

	if (app->a->len < 2 || !app->a->list || app->a->list->data == i)
		return (app->a->list->data == i);
	t1 = app->a->list->next;
	t2 = app->a->list->prev;
	while (t1 && t2 && t1 != t2 && t1->data != i && t2->data != i)
	{
		t1 = t1->next;
		t2 = t2->prev;
	}
	if (t1 == t2 && t1->data != i)
		return (0);
	r = 1;
	while (r > 0 && t1->data == i && app->a->list != t1)
		r = queue(app, RA);
	while (r > 0 && t2->data == i && app->a->list != t2)
		r = queue(app, RRA);
	return (r);
}

int	common_rotate_b_to(t_app *app, int i)
{
	t_list	*t1;
	t_list	*t2;
	int		r;

	if (app->b->len < 2 || !app->b->list || app->b->list->data == i)
		return (app->b->list->data == i);
	t1 = app->b->list->next;
	t2 = app->b->list->prev;
	while (t1 && t2 && t1 != t2 && t1->data != i && t2->data != i)
	{
		t1 = t1->next;
		t2 = t2->prev;
	}
	if (t1 == t2 && t1->data != i)
		return (0);
	r = 1;
	while (r > 0 && t1->data == i && app->b->list != t1)
		r = queue(app, RB);
	while (r > 0 && t2->data == i && app->b->list != t2)
		r = queue(app, RRB);
	return (r);
}

int	common_sort3(t_app *app)
{
	t_list	*a;
	int		r;

	a = app->a->list;
	r = 1;
	if (app->a->len != 3)
		;
	else if (a->next->data > a->data && a->next->data > a->prev->data)
		r = queue(app, RRA);
	else if (a->data > a->next->data && a->data > a->prev->data)
		r = queue(app, RA);
	a = app->a->list;
	if (app->a->len >= 2 && r && a->data > a->next->data)
		r = queue(app, SA);
	return (r);
}

void	common_debug(t_app *app, char *str, t_list *min, t_list *max)
{
	debug(app, "%s\n", str);
	if (min)
		debug(app, "min: %i\n", min->data);
	if (max)
		debug(app, "max: %i\n", max->data);
	debug_stacks(app);
}
