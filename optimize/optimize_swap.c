/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   optimize_swap.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/26 20:13:15 by rauer             #+#    #+#             */
/*   Updated: 2023/06/02 19:10:26 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../list/list.h"
#include "../operation/operation.h"
#include "../stack/stack.h"

int	optimize_sa3(t_stack *s, t_list *tmp)
{
	if (tmp->data == PB && tmp->next->data == PB)
	{
		tmp = tmp->next->next;
		while (tmp && (tmp->data == RA || tmp->data == RRA))
			tmp = tmp->next;
		if (tmp && tmp->data == SA)
		{
			tmp->data = SS;
			stack_pop(s);
			return (1);
		}
	}
	while (tmp && (tmp->data == RA || tmp->data == RRA))
		tmp = tmp->next;
	if (tmp && tmp->data == SB)
	{
		tmp->data = s->list->next->data;
		s->list->next->data = SS;
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_sa(t_stack *s)
{
	t_list	*tmp;

	tmp = s->list->next;
	if (tmp->data == SA)
		stack_pop(s);
	else if (tmp->data == SB)
		tmp->data = SS;
	else if (tmp->data == SS)
		tmp->data = SB;
	else if (tmp->next && tmp->next->next)
		return (optimize_sa3(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}

int	optimize_sb3(t_stack *s, t_list *tmp)
{
	if (tmp->data == PA && tmp->next->data == PA)
	{
		tmp = tmp->next->next;
		while (tmp && (tmp->data == RB || tmp->data == RRB))
			tmp = tmp->next;
		if (tmp && tmp->data == SB)
		{
			tmp->data = SS;
			stack_pop(s);
			return (1);
		}
	}
	return (-1);
}

int	optimize_sb(t_stack *s)
{
	t_list	*tmp;

	tmp = s->list->next;
	if (tmp->data == SB)
		stack_pop(s);
	else if (tmp->data == SA)
		tmp->data = SS;
	else if (tmp->data == SS)
		tmp->data = SA;
	else if (tmp->next && tmp->next->next)
		return (optimize_sb3(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}

int	optimize_ss(t_stack *s)
{
	if (s->list->next->data == SS)
	{
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == SA)
	{
		s->list->next->data = SB;
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == SB)
	{
		s->list->next->data = SA;
		stack_pop(s);
		return (1);
	}
	return (-1);
}
