/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   optimize.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/26 20:15:59 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 20:46:24 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPTIMIZE_H
# define OPTIMIZE_H
# include "../app/app.h"
# include "../operation/operation.h"
# include "../stack/stack.h"

void	optimize(t_stack *s);
int		optimize_sa(t_stack *s);
int		optimize_sb(t_stack *s);
int		optimize_ss(t_stack *s);
int		optimize_pa(t_stack *s);
int		optimize_pb(t_stack *s);
int		optimize_ra(t_stack *s);
int		optimize_rb(t_stack *s);
int		optimize_rr(t_stack *s);
int		optimize_rra(t_stack *s);
int		optimize_rrb(t_stack *s);
int		optimize_rrr(t_stack *s);

#endif
