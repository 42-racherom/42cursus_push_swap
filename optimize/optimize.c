/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   optimize.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/30 02:41:47 by racherom          #+#    #+#             */
/*   Updated: 2023/06/02 18:16:53 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/ft_printf.h"
#include "../list/list.h"
#include "../operation/operation.h"
#include "../stack/stack.h"
#include "optimize.h"

int	optimize_rra(t_stack *s)
{
	t_list	*tmp;

	if (s->list->next->data == RA)
	{
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == RR)
	{
		s->list->next->data = RB;
		stack_pop(s);
		return (1);
	}
	tmp = s->list->next;
	while (tmp && tmp->next && tmp->data == RRR)
		tmp = tmp->next;
	if (tmp && tmp->data == RRB)
	{
		tmp->data = RRR;
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_rrb(t_stack *s)
{
	t_list	*tmp;

	if (s->list->next->data == RB)
	{
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == RR)
	{
		s->list->next->data = RA;
		stack_pop(s);
		return (1);
	}
	tmp = s->list->next;
	while (tmp && tmp->next && tmp->data == RRR)
		tmp = tmp->next;
	if (tmp && tmp->data == RRA)
	{
		tmp->data = RRR;
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_rrr(t_stack *s)
{
	if (s->list->next->data == RR)
	{
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == RA)
	{
		s->list->next->data = RRB;
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == RB)
	{
		s->list->next->data = RRA;
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_f(t_stack *s)
{
	int	(*func[13])(t_stack *s);

	func[SA] = optimize_sa;
	func[SB] = optimize_sb;
	func[SS] = optimize_ss;
	func[PA] = optimize_pa;
	func[PB] = optimize_pb;
	func[RA] = optimize_ra;
	func[RB] = optimize_rb;
	func[RR] = optimize_rr;
	func[RRA] = optimize_rra;
	func[RRB] = optimize_rrb;
	func[RRR] = optimize_rrr;
	if (s->list->data >= SA && s->list->data <= RRR)
		return (func[s->list->data](s));
	return (-2);
}

void	optimize(t_stack *q)
{
	int	i;
	int	j;

	if (!q || q->len == 0)
		return ;
	i = 1;
	while (i > 0 && q->list)
	{
		i = -1;
		while (q->list->prev)
			stack_rrotate(q);
		while (i != 0 && q->list && q->list->next)
		{
			j = optimize_f(q);
			if (j > -1)
				i = j;
			stack_rotate(q);
		}
	}
}
