/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   optimize_pb.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/26 20:05:06 by rauer             #+#    #+#             */
/*   Updated: 2023/06/02 20:37:31 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/ft_printf.h"
#include "../operation/operation.h"
#include "../stack/stack.h"

int	optimize_pb6(t_stack *s, t_list *tmp)
{
	if (tmp->next->data == PB && tmp->next->next->next->next->data == PA
		&& tmp->next->next->next->data == PA && tmp->next->next->data == SB)
	{
		tmp->next->next->data = SA;
		tmp->next->next->next->data = RA;
		tmp->next->next->next->next->data = SA;
		stack_pop(s);
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_pb5(t_stack *s, t_list *tmp)
{
	if (tmp->data == PB && tmp->next->data == SB && tmp->next->next->data == PA
		&& tmp->next->next->next->data == PA)
	{
		tmp->next->next->next->data = SA;
		stack_pop(s);
		stack_pop(s);
		stack_pop(s);
	}
	else if (tmp->next->next->next->next && tmp->data == RA)
		return (optimize_pb6(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}

int	optimize_pb3(t_stack *s, t_list *tmp)
{
	if (tmp->data == RA && tmp->next->data == PA)
	{
		tmp->data = SA;
		tmp->next->data = RA;
	}
	else if (tmp->data == RRA && tmp->next->data == PA)
	{
		tmp->data = RRA;
		tmp->next->data = SA;
	}
	else if (tmp->data == SB && tmp->next->data == RB)
	{
		tmp->data = RB;
		tmp->next->data = PB;
	}
	else if (tmp->next->next && tmp->next->next->next)
		return (optimize_pb5(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}

int	optimize_pb(t_stack *s)
{
	t_list	*tmp;

	tmp = s->list->next;
	if (tmp->data == PA)
		stack_pop(s);
	else if (tmp->next)
		return (optimize_pb3(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}
