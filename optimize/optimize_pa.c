/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   optimize_pa.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/26 20:05:06 by rauer             #+#    #+#             */
/*   Updated: 2023/06/02 18:14:59 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../operation/operation.h"
#include "../stack/stack.h"

int	optimize_pa5(t_stack *s, t_list *tmp)
{
	if (tmp->data == PA && tmp->next->data == SA && tmp->next->next->data == PB
		&& tmp->next->next->next->data == PB)
	{
		tmp->next->next->next->data = SB;
		stack_pop(s);
		stack_pop(s);
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_pa3(t_stack *s, t_list *tmp)
{
	if (tmp->data == RB && tmp->next->data == PB)
	{
		tmp->data = SB;
		tmp->next->data = RB;
	}
	else if (tmp->data == RRB && tmp->next->data == PB)
	{
		tmp->data = RRB;
		tmp->next->data = SB;
	}
	else if (tmp->data == SA && tmp->next->data == RA)
	{
		tmp->data = RA;
		tmp->next->data = PA;
	}
	else if (tmp->next->next && tmp->next->next->next)
		return (optimize_pa5(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}

int	optimize_pa(t_stack *s)
{
	t_list	*tmp;

	tmp = s->list->next;
	if (tmp->data == PB)
	{
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	if (tmp->next)
		return (optimize_pa3(s, tmp));
	return (-1);
}
