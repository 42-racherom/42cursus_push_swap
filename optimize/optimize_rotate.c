/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   optimize_rotate.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/26 20:05:02 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 20:33:36 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../list/list.h"
#include "../operation/operation.h"
#include "../stack/stack.h"

int	optimize_ra3(t_stack *s, t_list *tmp)
{
	if (tmp->data == PA && tmp->next->data == RRA)
	{
		tmp->next->data = SA;
		stack_pop(s);
		return (1);
	}
	else if (tmp->data == PB && tmp->next->data == RRA)
	{
		tmp->data = SA;
		tmp->next->data = PB;
		stack_pop(s);
		return (1);
	}
	while (tmp && tmp->next && tmp->data == RR)
		tmp = tmp->next;
	if (tmp && tmp->data == RB)
	{
		tmp->data = RR;
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_ra(t_stack *s)
{
	t_list	*tmp;

	tmp = s->list->next;
	if (tmp->data == RRA)
		stack_pop(s);
	else if (tmp->data == RRR)
		tmp->data = RRB;
	else if (tmp->next)
		return (optimize_ra3(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}

int	optimize_rb3(t_stack *s, t_list *tmp)
{
	if (tmp->data == PB && tmp->next && tmp->next->data == RRB)
	{
		tmp->next->data = SB;
		stack_pop(s);
		return (1);
	}
	else if (tmp->data == PA && tmp->next && tmp->next->data == RRB)
	{
		tmp->data = SB;
		tmp->next->data = PA;
		stack_pop(s);
		return (1);
	}
	while (tmp && tmp->next && tmp->data == RR)
		tmp = tmp->next;
	if (tmp && tmp->data == RA)
	{
		tmp->data = RR;
		stack_pop(s);
		return (1);
	}
	return (-1);
}

int	optimize_rb(t_stack *s)
{
	t_list	*tmp;

	tmp = s->list->next;
	if (tmp->data == RRB)
		stack_pop(s);
	else if (tmp->data == RRR)
		tmp->data = RRA;
	else if (tmp->next)
		return (optimize_rb3(s, tmp));
	else
		return (-1);
	stack_pop(s);
	return (1);
}

int	optimize_rr(t_stack *s)
{
	if (s->list->next->data == RRR)
	{
		stack_pop(s);
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == RRA)
	{
		s->list->next->data = RB;
		stack_pop(s);
		return (1);
	}
	else if (s->list->next->data == RRB)
	{
		s->list->next->data = RA;
		stack_pop(s);
		return (1);
	}
	return (-1);
}
