/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: racherom <racherom@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 03:33:46 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 09:56:30 by racherom         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GNL_H
# define GNL_H
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 12
# endif
# include <stdlib.h>
# include <unistd.h>

typedef struct s_buffer
{
	int		fd;
	int		size;
	int		start;
	char	*buffer;
}		t_buffer;

char	*gnl(int fd);

#endif