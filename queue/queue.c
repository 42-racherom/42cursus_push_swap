/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   queue.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/09 18:28:20 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 20:47:32 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../app/app.h"
#include "../debug/debug.h"
#include "../list/list.h"
#include "../operation/operation.h"
#include "../stack/stack.h"

int	queue(t_app *app, t_operation o)
{
	if ((app->flags & DEBUG) == 3)
		debug_stacks(app);
	if ((app->flags & DEBUG) > 1)
		operation_print(o);
	if (!operation_execute(app, o))
		return (0);
	while (app->q->list && app->q->list->next)
		stack_rotate(app->q);
	if (app->q->list && app->q->list->data == (int)operation_opposite(o))
		stack_pop(app->q);
	else if (!stack_append(app->q, o))
		operation_print(o);
	return (1);
}

void	queue_print(t_app *app)
{
	int	i;

	i = app->q->len;
	while (i > 0 && app->q->list->prev)
		stack_rrotate(app->q);
	while ((app->flags & DEBUG) < 2 && app->q->len > 0)
		operation_print(stack_pop(app->q));
	debug(app, "Operations: %i\n", i);
}
