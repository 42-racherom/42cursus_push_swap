/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   queue.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/18 13:52:47 by rauer             #+#    #+#             */
/*   Updated: 2023/05/26 20:17:55 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef QUEUE_H
# define QUEUE_H

# include "../app/app.h"
# include "../debug/debug.h"
# include "../ft_printf/ft_printf.h"
# include "../list/list.h"
# include "../operation/operation.h"
# include "../stack/stack.h"

int		queue(t_app *app, t_operation o);
void	queue_print(t_app *app);
#endif
