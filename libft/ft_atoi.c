/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/17 19:12:25 by rauer             #+#    #+#             */
/*   Updated: 2023/01/19 00:26:21 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int	ft_is_whitespace(char c)
{
	return ((c >= 9 && c <= 13) || c == ' ');
}

int	ft_atoi(const char *str)
{
	int		i;
	int		m;

	i = 0;
	m = 1;
	while (ft_is_whitespace(*str))
		str++;
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		m = -1;
		str++;
	}
	while (*str >= '0' && *str <= '9')
		i = (i * 10) + (m * (*(str++) - '0'));
	return (i);
}
