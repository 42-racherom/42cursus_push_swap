/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rauer <rauer@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/18 16:43:14 by rauer             #+#    #+#             */
/*   Updated: 2023/01/20 14:38:40 by rauer            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*ptr;
	size_t	m;

	m = size * count;
	if (size != 0 && m / size != count)
		return (NULL);
	ptr = malloc(m);
	if (!ptr)
		return (NULL);
	ptr = ft_memset(ptr, 0, m);
	return (ptr);
}
