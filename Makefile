# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rauer <rauer@student.42roma.it>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/03/22 20:19:47 by rauer             #+#    #+#              #
#    Updated: 2023/06/02 20:44:58 by rauer            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap
SRC = push_swap.c
OBJ = $(SRC:.c=.o)
LIB = list stack ft_printf app debug operation queue optimize common libft
LIB_FILES = $(addprefix inc/lib, $(addsuffix .a, $(LIB)))
SORT_LIB = quick insertion utils
SORT_LIB_FILES = $(addprefix inc/lib, $(addsuffix .a, $(SORT_LIB)))
LIB_FLAGS = -Linc $(addprefix -l, $(LIB) $(SORT_LIB)) $(addprefix -I, $(LIB) $(SORT_LIB))
ALL_LIB = list stack ft_printf app debug operation gnl queue optimize common libft
ALL_LIB_FILES = $(addprefix inc/lib, $(addsuffix .a, $(ALL_LIB)))
BONUS = checker
BONUS_SRC = checker.c
BONUS_OBJ = $(BONUS_SRC:.c=.o)
BONUS_LIB = list stack ft_printf app debug operation gnl utils libft
BONUS_LIB_FILES = $(addprefix inc/lib, $(addsuffix .a, $(BONUS_LIB)))
BONUS_LIB_FLAGS = -Linc $(addprefix -l, $(BONUS_LIB)) $(addprefix -I, $(BONUS_LIB))
GCC = gcc -Wall -Wextra -Werror -g3

all: $(NAME) $(BONUS)

$(NAME): $(OBJ) $(LIB_FILES) $(SORT_LIB_FILES) inc
	$(GCC) $(LIB_FLAGS) $(OBJ) -o $@

$(BONUS): $(BONUS_OBJ) $(BONUS_LIB_FILES) inc
	$(GCC) $(BONUS_LIB_FLAGS) $(BONUS_OBJ) -o $@

bonus: $(BONUS)
mandantory: $(NAME)

check: args500.txt
check: args = $(shell cat args500.txt)
check: all
	./push_swap $(args) | tee out.txt | ./checker $(args)

.PHONY: all bonus check mandantory clean fclean

$(ALL_LIB_FILES): inc/lib%.a: inc FORCE
	@make -C $* LIB_PATH=../inc/lib

$(SORT_LIB_FILES): inc/lib%.a: inc FORCE
	@make -C sort/$* LIB_PATH=../../inc/lib

inc:
	mkdir inc

FORCE: ;

%.o: %.c
	$(GCC) -c $< -o $@
argsr.txt:
	ruby -e "puts (0..499).to_a.reverse.join(' ')" > $@
args%r.txt:
	ruby -e "puts (1..$*).to_a.reverse.join(' ')" > $@
args%.txt:
	ruby -e "puts (1..$*).to_a.shuffle.join(' ')" > $@
run: run500

run%: args%.txt $(NAME)
	cat $< | xargs ./push_swap > out.txt

drun%: args%.txt $(NAME)
	cat $< | xargs ./push_swap -dd > out.txt

re_run: re run
viz: viz500
viz%: args%.txt $(NAME)
	python3 pyviz.py `cat $<`
re_viz: re viz
norm: $(SRC)
	norminette $^

clean_%:
	make -C $* clean
sort_clean_%:
	make -C sort/$* clean

clean: $(addprefix clean_, $(ALL_LIB)) $(addprefix sort_clean_, $(SORT_LIB))
	rm -rf $(OBJ) $(BONUS_OBJ)

aclean:
	rm -f args*.txt

fclean_%:
	make -C $* fclean
sort_fclean_%:
	make -C sort/$* fclean

fclean: clean aclean
	rm -f $(NAME) $(BONUS)
	rm -rf inc
re: fclean all
